package pt.isec.gps_g22.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CylinderVolumeController {

    @FXML
    private TextField raioField;

    @FXML
    private TextField alturaField;

    @FXML
    private Label resultadoLabel;

    public void calcularVolume(ActionEvent actionEvent) {

        try {
            double raio = Double.parseDouble(raioField.getText());
            double altura = Double.parseDouble(alturaField.getText());

            double volume = Math.PI * Math.pow(raio, 2) * altura;

            resultadoLabel.setText("Volume: " + String.format("%.2f", volume));

        } catch (NumberFormatException e) {

            resultadoLabel.setText("Please, insert valid values.");
        }

    }
}
