package pt.isec.gps_g22;

import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import javafx.fxml.FXMLLoader;

import javafx.application.Application;

import java.io.IOException;
import java.util.Objects;

public class CalculatorApplication extends Application
{
    @Override
    public void start(Stage stage) throws IOException
    {
        VBox main_view = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("main-view.fxml")));
        
        Scene scene = new Scene(main_view, 600, 600);

        stage.setTitle("Calculator");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args)
    {
        launch();
    }
}