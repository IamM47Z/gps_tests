module pt.isec.gps_g22 {
    requires javafx.controls;
    requires javafx.fxml;

    opens pt.isec.gps_g22 to javafx.fxml;
    exports pt.isec.gps_g22;
    exports pt.isec.gps_g22.controllers;
    opens pt.isec.gps_g22.controllers to javafx.fxml;
}